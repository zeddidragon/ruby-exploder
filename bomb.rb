require_relative 'diffuse_explosion'
require_relative 'entity'

class Bomb < Entity
	def initialize damage, power
		super()
		@damage, @power = damage, power
		@direction = :up
		@symbol = 'B'
	end

	attr_accessor :direction, :fuseTime, :damage, :power

	def tick
		explode! if shouldExplode?
	end

	def prime!
	end
	
	def shouldExplode?
		true
	end	

	def explode!
		explosion = DiffuseExplosion.new @position, @power
		$map.explosion = explosion.spread!
		$map.bombs.delete self

#		explosion = Hash.new(0)
#		seed = Explosion.new @position, @power, @direction
#
#		allFlares = queue = [seed]
#		while not queue.empty?
#			currentRow = queue
#			queue = []
#			currentRow.each do |flare|
#				queue << flare.spread!
#			end
#			queue = flatten queue
#			allFlares << queue
#		end
#
#		allFlares.flatten!
#		allFlares.each do |flare|
#			explosion[flare.position] += flare.power if flare
#		end
#
#		explosion.each_pair do |position, power|
#			explosion[position] = (power * @damage).ceil
#		end
#		$map.explosion = explosion
#
#		$map.bombs.delete self
	end

	def flatten queue
		hash = Hash.new(0)
		queue.flatten.compact.each do |flare|
			hash[{pos: flare.position, dir: flare.direction}] += flare.power
		end
		hash.map{|key, value| Explosion.new(key[:pos], value, key[:dir])} || []
	end
end

class TimedBomb < Bomb
	def initialize damage, power, fuseTime = 5
		super damage, power
		@maxFuseTime = fuseTime
	end

	attr_accessor :maxFuseTime, :fuseTime

	def tick
		@fuseTime -= 1
		@symbol = @fuseTime
		explode! if shouldExplode?
	end

	def prime!
		@fuseTime = @maxFuseTime
	end

	def shouldExplode?
		@fuseTime <= 0
	end	
end