class Random
	def self.up_to max
		rand max
	end

	def self.in_range x1, x2
		return x1 if x1 == x2
		max = [x1, x2].max
		min = [x1, x2].min
		rand(max - min) + min
	end

	def self.true?
		rand(2) == 1
	end
end