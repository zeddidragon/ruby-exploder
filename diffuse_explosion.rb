# Attempt #2 at explosion algorithm

# - Create a hash with gridpoints as keys and power as value
# - Plant the seed
# - Loop
# 	- For every explosion in the hash
# 		- Check adjacent tiles in both the map and the hash
#   	- If the adjacent tiles are clear
# 			- For every adjacent tile that contains no more than the tile's power - minimumdifference
# 				- Spread to those tiles
# 	- Break if we went the entire loop without any spreads
#
# Should work well for vaporizer style bombs, but how can this accomodate directional blasts?

class DiffuseExplosion
	
	def initialize position, power, dummyarg
		@hash = Hash.new(0)
		@hash[position] = power
		@minimumDifference = 2
	end

	def spread!
		begin
			@additions = Hash.new(0)
			did_something = false
			@hash.each_pair do |position, power|
				new_additions = diffuseSpread!(validNeighbours(position, power), position, power)
				if not new_additions.empty? and not new_additions.all_values? 0
					did_something ||= true
					@additions += new_additions
				end
			end
			@hash += @additions
		end while did_something
		$map.explosion = @hash
	end

	protected
		def validNeighbours position, power
			ret = Hash.new(0)
			Direction::each do |direction|
				new_position = Direction::move_in position, direction
				if $map[new_position] == :empty
					ret[new_position] = @hash[new_position] + @additions[new_position]
				end
			end
			while not ret.empty? and (max = ret.values.max) > power - @minimumDifference * ret.size
				ret.delete_if{|key, value| value == max}
			end

			ret.keys
		end

		# Find the neighbour(s) with the smallest value
		# if possible:
		#	Fill them up until they reach the value of the next smallest neighbour
		# else:
		# 	Fill them up as much as can be done, then quit
		# Repeat until no more power can be drained

		def diffuseSpread! neighbours, position, power
			new_additions = Hash.new(0)
			return {} if neighbours.empty?
			while true
				# Find the neighbours(s) with the smallest value
				allNeigbours = Hash[neighbours.map{|pos| [pos, @hash[pos] || 0 + @additions[pos] || 0 + new_additions[pos] || 0 ]}]
				minNeighbours = allNeigbours.all_minimum
				secondSmallestValue = allNeigbours.values.uniq.sort[1]
				difference = secondSmallestValue || 0 - minNeighbours.first.last
				totalDifference = difference * minNeighbours.size
				# if possible
				if secondSmallestValue and (power - secondSmallestValue - @minimumDifference) > totalDifference
					# Fill them up until they reach the value of the next smallest neighbour
					power -= totalDifference
					minNeighbours.each_key do |key|
						new_additions[key] += difference
					end
				else
					# Fill them up as much as can be done, then quit
					difference = [(power - minNeighbours.first.last - @minimumDifference) / (minNeighbours.size + 1), 0].max
					power -= difference * minNeighbours.size
					minNeighbours.each_key do |key|
						new_additions[key] += difference
					end
					new_additions[position] = power - (@hash[position])
					return new_additions
				end
			end
		end
end