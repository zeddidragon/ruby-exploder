class Direction

	Directions = [ 
		:south_west, :west, :north_west,
		:north_east, :east, :south_east,
	]

	def self.each
		Directions.each do |dir|
			yield dir
		end
	end

	def self.left_of dir, amount = 1
		Directions[ Directions.find_index(dir) - amount ]
	end

	def self.right_of dir, amount = 1
		Directions[ (Directions.find_index(dir) + amount) % Directions.length ]
	end

	def self.reverse dir 
		left_of dir, 3
	end

	def self.move_in pos, dir 
		case dir
		# North
		when :north_west
			{
				x: pos[:x] - (pos[:y] + 1) % 2,
				y: pos[:y] - 1
			}
		when :north_east
			{
				x: pos[:x] + (pos[:y]) % 2,
				y: pos[:y] - 1
			}

		# South
		when :south_west
			{
				x: pos[:x] - (pos[:y] + 1) % 2,
				y: pos[:y] + 1
			}
		when :south_east
			{
				x: pos[:x] + (pos[:y]) % 2,
				y: pos[:y] + 1
			}

		# Sideways
		when :west
			{
				x: pos[:x] - 1,
				y: pos[:y]
			}
		when :east
			{
				x: pos[:x] + 1,
				y: pos[:y]
			}
		end
	end

	# General direction from point p1 to p2
	# returns a hash with the hex directions left and right of the vector from p1 to p2
	# will only return a hash with the key :straight if the path is direct
	#
	#   :north_west     :north_east
	#            \     /
	#   x<y/2     \   /   x>-(y/2)
	#              \ /
	#   :west  ---- @ ---- :east
	#              / \
	#   x<(-y/2)  /   \   x>y/2
	#            /     \
	#   :south_west     :south_east
	#
	# We want to find which sector or line p2 is at. However, because we're dealing
	# purely with integer indices in a 2d array, things get a bit more dicey than that.
	#
	# There are two possibilties for p1
	# 
	# Instance 1, p1 is on an odd line
	#  -1  0 ++1
	#  [\][ ][/]   2n+1	-2
	# [ ][\][/]    2n+2	-1
	#  [-][@][-]   2n+3	 0
	# [ ][/][\]    2n+4	+1
	#  [/][ ][\]   2n+5	+2
	#  -1  0 +1
	# 
	# Instance 2, p1 is on an even line
	#  -1 0 +1
	# [\][ ][/]    2n+0	-2
	#  [\][/][ ]   2n+1	-1
	# [-][@][-]    2n+2	 0
	#  [/][\][ ]   2n+3	+1
	# [/][ ][\]    2n+4	+2
	#  -1 0 +1
	#
	# The formula for the two diagonals change a little for each instance.
	# Remember that 1/2 = 0 and -1/2 = -1
	# Rounding down with negative numbers means going away from zero!
	# When p1 is on an odd line
	#   nw/se is defined by x =   (y+1)/2
	#   sw/ne is defined by x = - (y   /2)
	# When p1 is on an even line
	#   nw/se is defined by x =    y   /2
	#   sw/ne is defined by x = -((y+1)/2)

	def self.general point1, point2 = nil
		# Check direction from origo to point1 if only point1 is supplied
		if not point2
			point2 = point1
			point1 = {x: 0, y: 0}
		end

		xDif = point2[:x] - point1[:x]
		yDif = point2[:y] - point1[:y]

		south_east = north_west =   (yDif +  point1[:y] % 2) / 2
		south_west = north_east = -((yDif + (point1[:y] + 1) % 2) / 2)

		if    yDif  < 0 # North-ish
			if    xDif  <  north_west then { left: :west,       right: :north_west }
			elsif xDif ==  north_west then { straight: :north_west }
			elsif xDif  <  north_east then { left: :north_west, right: :north_east }
			elsif xDif ==  north_east then { straight: :north_east }
			else                           { left: :north_east, right: :east }
			end

		elsif yDif == 0 # Straight horizontal line
			if    xDif  < 0           then { straight: :west }
			elsif xDif == 0           then { straight: :up }
			else                           { straight: :east }
			end

		else            # South-ish
			if    xDif  <  south_west then { left: :south_west, right: :west }
			elsif xDif ==  south_west then { straight: :south_west }
			elsif xDif  <  south_east then { left: :south_east, right: :south_west }
			elsif xDif ==  south_east then { straight: :south_east }
			else                           { left: :east,       right: :south_east }
			end
		end
	end
end