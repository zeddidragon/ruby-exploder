# map.rb
# Handles maps and the generation of maps
require_relative 'helpers'
require_relative 'random'

class Map
	def initialize(width = 32, height = 32)
		@width, @height = width, height
		clear!

		@enemies = []
		@bombs = []

		@offset = {x: 0, y: 0}
	end

	attr_accessor :player, :tiles, :enemies, :bombs, :explosion, :offset

	def tick
		@explosion = nil if @explosion

		[@enemies, @bombs].each do |group|
			group.each do |entity|
				entity.tick
			end
		end
	end

	def print
		view = getView({x: @player.x - 10, y: player.y - 10})
		offset = view.offset

		[@enemies, @bombs].each do |group|
			group.each do |entity|
				view[entity.position - offset] = entity.symbol if view.contains entity.position
			end
		end

		@explosion.each_pair do |position, damage|
			view[position - offset] = $ShowDamage ? "%.0f" % damage : '*' if view.contains(position) && damage > 0
		end if @explosion

		view[player.position - offset] = '@' if not @player.nil? and view.contains @player.position
		playerOffset = (player.position - offset)[:y] == 10 ? (offset[:y] + 1) % 2 : 0

		view.tiles.each_with_index do |row, index|
			rowOffset = (index + offset[:y]) % 2

			row = row.map{|tile| "%#{$ShowDamageWidth}s" % tile } if $ShowDamage
			puts (' ' * (rowOffset + playerOffset)) + (row.join ' ')
		end

		puts @explosion.values.sum if @explosion
		puts $error_messages
		$error_messages = []
	end

	def getView start = {x: 0, y: 0}, frame = {width: 21, height: 21}
		width = frame[:width]
		height = frame[:height]
		start[:x] = x = start[:x].clamp(0, @width - width)
		start[:y] = y = start[:y].clamp(0, @height - height)

		ret = Map.new width, height
		ret.offset = start
		@tiles.each_in_range_with_zero_index (y..y+height-1) do |row, j|
			row.each_in_range_with_zero_index (x..x+width-1) do |tile, i|
				ret[{x: i, y:j}] = tile
			end
		end
		visualize(ret)
	end

	Tiles = { empty: '.', wall: 'O', water: '~'}

	def visualize view
		view.tiles.each do |row|
			row.each_with_index do |tile, i|
				row[i] = Tiles[tile]
			end
		end
		view
	end

	def [] point
		@tiles[point[:y]][point[:x]]
	end

	def []= point, value
		@tiles[point[:y]][point[:x]] = value
	end

	def contains position
		return (position[:x].between?(@offset[:x], @offset[:x] + @width - 1)) && (position[:y].between?(@offset[:y], @offset[:y] + @height - 1))
	end

	def generate! minPaths = 32, maxPaths = 64
		tries = Random::in_range(minPaths, maxPaths)

		fill!
		snakePaths! tries
	end

	def snakePaths! tries, tile = :empty
		startPoint = randomPoint
		endPoint = randomPoint

		(0..tries).each do
			snakePath! startPoint, endPoint, tile
		end
	end

	def snakePath! startPoint, endPoint, tile = :empty, tries = 10
		fromPoint = startPoint

		while (tries-=1) > 0 and fromPoint != endPoint  do
			toPoint = randomPoint fromPoint, endPoint
			makePath! fromPoint, toPoint, tile
			fromPoint = toPoint
		end

		# Smash a final path just to make sure
		makePath! fromPoint, endPoint, tile
	end

	def makePath! fromPoint, endPoint, tile = :empty
		dir = Direction::general fromPoint, endPoint

		if dir[:straight]
			straightPath! fromPoint, endPoint, dir[:straight], tile
		else
			firstDir, secondDir = Random::true? ? dir.values : dir.values.reverse

			point = fromPoint
			while Direction::general(point, endPoint)[:straight] != secondDir
				self[point] = tile
				point = Direction::move_in point, firstDir
			end
			straightPath! point, endPoint, secondDir, tile
		end
	end

	def straightPath! point, endPoint, direction, tile = :empty
		while point != endPoint
			self[point] = tile
			point = Direction::move_in point, direction
		end
		self[point] = tile
	end

	def fill! tile = :wall
		@tiles = Array.new(@height) {Array.new(@width, tile)}
	end

	def flood!
		fill! :water
	end

	def clear!
		fill! :empty
	end

	def randomPoint(p1 = {x: 0, y: 0}, p2 = {x: @width-1, y: @height-1}) {
		x: Random::in_range(p1[:x], p2[:x]),
		y: Random::in_range(p1[:y], p2[:y]),
	}
	end
end