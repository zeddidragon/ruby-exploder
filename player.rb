require_relative 'entity'
require_relative 'bomb'

class Player < Entity
	def initialize
		super
		@symbol = '@'
		@bomb = TimedBomb.new $damage, $power
		@bomb.direction = :west if $WestBomb
		@position = {x: 10, y: 10}
		@bomb.maxFuseTime = 2
	end

	def turn input
		case input
		#Hexagonal movement
		#
		#   W E
		#  A @ D
		#   Z X
		#
		# @ is the player
		# S waits
		# The surrounding keys move

		# Movement
			# North
			when 'w'
				move :north_west
			when 'e'
				move :north_east


			# South
			when 'z'
				move :south_west
			when 'x'
				move :south_east

			# Sideways
			when 'a'
				move :west
			when 'd'
				move :east
		# /Movement

		# Plant bomb
		when 's'
			plant @bomb

		# System commands
		when 'Q'
			abort

		else
			#do nothing
		end
	end

	def plant bomb
		bomb.position = position
		$map.bombs << bomb
		bomb.prime!
	end
end