require_relative 'directions'

class Entity
	def initialize
		@position = {x: 0, y: 0}
		@symbol = '?'
	end

	def move direction
		@position = Direction::move_in(@position, direction)
	end

	attr_accessor :position, :symbol

	def x
		@position[:x]
	end
	def y
		@position[:y]
	end
	def x= value
		@position[:x] = value
	end
	def y= value
		@position[:y] = value
	end
end