# helpers.rb

class Array
	def each_in_range range 
		range.each do |index|
			yield self[index]
		end
	end

	def each_in_range_with_zero_index range 
		i = 0
		range.each do |index|
			yield self[index], i
			i += 1
		end
	end

	def sum
		sum = 0
		self.each do |v|
			sum += v
		end
		sum
	end
end

class Hash
	def + other
		ret = Hash.new {0}

		self.each_pair do |key, value|
			ret[key] += value
		end
		other.each_pair do |key, value|
			ret[key] += value
		end

		ret
	end
	
	def - other
		ret = Hash.new {0}
		
		self.each_pair do |key, value|
			ret[key] += value
		end
		other.each_pair do |key, value|
			ret[key] -= value
		end

		ret
	end

	# Returns true if each value in the hash is the same
	def is_flat?
		current_value = self.first[1]
		all_values? current_value
	end

	def all_values? value
		self.each_value do |v|
			return false if value != v
		end
		true
	end

	def all_with_value value
		ret = {}
		self.each do |k, v|
			ret[k] = v if v == value
		end
		return ret
	end

	def all_minimum
		all_with_value self.values.min
	end
end

class Numeric
	def clamp min, max
		[[self, max].min, min].max
	end
end
