# explosion.rb 

require_relative 'directions'

# Class assumes there is a global variable available called $map, which it can access for tiles
class Explosion

	def initialize position, power, direction
		@position, @power, @direction = position, power, direction
	end

	attr_accessor :position, :power, :direction

	def spread!
		return nil if @power < 1
		@direction == :up ? circleSpread! : fanSpread!
	end

	def circleSpread!
		ret = []
		remainder = @power * (1 - $SpreadRatio)**2
		power = @power - remainder
		Direction::Directions.each do |dir|
			ret << singleSpread(power / 6, dir)
		end
		@power = remainder
		ret.flatten
	end

	def fanSpread!
		ret = []
		ratio = 1.0/3
		power = @power * $SpreadRatio
		{
			@direction                        => power * ratio,
			(Direction::left_of( @direction)) => power * ratio,
			(Direction::right_of(@direction)) => power * ratio,
		}.each_pair do |dir, pow|
			ret << singleSpread(pow, dir)
		end
		@power -= power
		ret.flatten
	end

	def singleSpread power, direction
		position = Direction::move_in @position, direction
		return nil if not $map.contains position

		if $map[position] ==  :empty
			Explosion.new position, power, @direction == :up ? direction : @direction
		else
			[
				Explosion.new(@position, power/3, Direction::left_of( direction)),
				Explosion.new(@position, power/3, Direction::right_of(direction)),
				Explosion.new(@position, power/3, Direction::reverse( direction)),
			]
		end
	end
end